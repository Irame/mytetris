package Game;

import Game.Tetriminos.*;

/**
 * Created by Felix on 26.05.2014.
 */
public class GameLoop implements Runnable {
    public boolean isRunning;
    public Tetrimino curTetrimino;
    private double speed;
    private Game game;

    public GameLoop(Game game) {
        this.game = game;
        this.speed = game.getSpeed();
    }

    @Override
    public void run() {
        while (isRunning) {
            curTetrimino = createRandomTetrimino();
            for (int i = 0; i < 4; i++) {
                if (!curTetrimino.moveDown()) {
                    break;
                }
            }

            do {
                easySleep((int) (1000 / speed));
            } while (curTetrimino.moveDown());

            deleteForFullRows();

            if (curTetrimino.getTopBorder() < 0) {
                System.out.println("GAME OVER");
                curTetrimino = null;
                isRunning = false;
            }
        }
    }

    private void deleteForFullRows() {
        int[] rowBlockCount = new int[game.getGameField().GAME_FIELD_HEIGHT];
        for (Position position : game.getGameField().getGameFieldBlockMap().keySet()) {
            if (position.yPos >= 0 && ++rowBlockCount[position.yPos] == game.getGameField().GAME_FIELD_WIDTH)
                deleteRow(position.yPos);
        }

    }

    private void deleteRow(int rowToDelete) {
        for (int col = 0; col < game.getGameField().GAME_FIELD_WIDTH; col++) {
            game.getGameField().getGameFieldBlockMap().remove(new Position(col, rowToDelete)).hide();
        }
        for (int row = rowToDelete - 1; row >= 0; row--) {
            for (int col = 0; col < game.getGameField().GAME_FIELD_WIDTH; col++) {
                Position blockToMove = new Position(col, row);
                if (game.getGameField().getGameFieldBlockMap().containsKey(blockToMove))
                    game.getGameField().getGameFieldBlockMap().get(blockToMove).setPos(new Position(col, row + 1));
            }
        }
    }

    private Tetrimino createRandomTetrimino() {
        Position startPosition = new Position(5, -4);
        int rotation = (int) (Math.random() * 4);
        switch ((int) (Math.random() * 7)) {
            case 0:
                return new I(game.getGameField(), startPosition, rotation);
            case 1:
                return new J(game.getGameField(), startPosition, rotation);
            case 2:
                return new L(game.getGameField(), startPosition, rotation);
            case 3:
                return new O(game.getGameField(), startPosition, rotation);
            case 4:
                return new S(game.getGameField(), startPosition, rotation);
            case 5:
                return new T(game.getGameField(), startPosition, rotation);
            case 6:
                return new Z(game.getGameField(), startPosition, rotation);
        }
        return null;
    }

    private void easySleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
