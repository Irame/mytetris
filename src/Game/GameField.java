package Game;

import Game.Tetriminos.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Felix on 20.05.2014.
 */
public class GameField {
    public final int GAME_FIELD_WIDTH = 10;
    public final int GAME_FIELD_HEIGHT = 16;

    private Parent gameField;
    private GridPane gameGrid;
    private ConcurrentHashMap<Position, Block> blockMap;

    public GameField() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("gameField.fxml"));
        try {
            gameField = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        gameGrid = ((GameFieldController) fxmlLoader.getController()).gameGrid;

        blockMap = new ConcurrentHashMap<Position, Block>();
    }

    public Block createBlock(Position position) {
        return new Block(position, this);
    }

    public GridPane getGameGrid() {
        return gameGrid;
    }

    public Parent getGameFieldParent() {
        return gameField;
    }

    public ConcurrentHashMap<Position, Block> getGameFieldBlockMap() {
        return blockMap;
    }

    public Block getBlock(Position pos) {
        return blockMap.get(pos);
    }

    public I createI(Position position, int rotation) {
        return new I(this, position, rotation);
    }

    public J createJ(Position position, int rotation) {
        return new J(this, position, rotation);
    }

    public L createL(Position position, int rotation) {
        return new L(this, position, rotation);
    }

    public O createO(Position position, int rotation) {
        return new O(this, position, rotation);
    }

    public S createS(Position position, int rotation) {
        return new S(this, position, rotation);
    }

    public T createT(Position position, int rotation) {
        return new T(this, position, rotation);
    }

    public Z createZ(Position position, int rotation) {
        return new Z(this, position, rotation);
    }
}
