package Game;

import Game.Tetriminos.Position;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Felix on 21.05.2014.
 */
public class Block {
    private Position pos;
    private GameField gameField;
    private Rectangle rectangle;
    private boolean displayed;

    public Block(Position position, GameField gameField) {
        this.pos = position;
        this.gameField = gameField;
        rectangle = new Rectangle();
        rectangle.widthProperty().bind(gameField.getGameGrid().widthProperty().divide(gameField.getGameGrid().getColumnConstraints().size()).subtract(1));
        rectangle.heightProperty().bind(gameField.getGameGrid().heightProperty().divide(gameField.getGameGrid().getRowConstraints().size()).subtract(1));
        rectangle.setFill(Color.BLACK);
        displayed = false;
        gameField.getGameFieldBlockMap().put(pos, this);
        redraw();
    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position position) {
        System.out.println(this + " setPos: \t" + this.pos + " => " + position);
        gameField.getGameFieldBlockMap().put(position, gameField.getGameFieldBlockMap().remove(this.pos));
        this.pos = position;

        redraw();
    }

    public void setColor(Color color) {
        Platform.runLater(() -> {
            rectangle.setFill(color);
        });
    }

    private void redraw() {
        if (displayed) {
            //System.out.println(this + ": Remove from " + this.pos);
            //removing node from the old position
            Platform.runLater(new Draw(null));
            displayed = false;
        }

        //adding node to new position
        if (this.pos.xPos >= 0 && this.pos.xPos < gameField.GAME_FIELD_WIDTH && this.pos.yPos >= 0 && this.pos.yPos < gameField.GAME_FIELD_HEIGHT) {
            //System.out.println(this + ": Add to " + position);
            Platform.runLater(new Draw(pos));
            displayed = true;
        }
    }

    public void hide(){
        Platform.runLater(new Draw(null));
    }

    public void show(){
        Platform.runLater(new Draw(pos));
    }

    private class Draw implements Runnable {
        private Position position;

        Draw(Position position) {
            this.position = position;
        }

        @Override
        public void run() {
            if (position == null) {
                gameField.getGameGrid().getChildren().remove(rectangle);
            } else {
                gameField.getGameGrid().add(rectangle, position.xPos, position.yPos);
            }
        }
    }
}
