package Game.Tetriminos;

/**
 * Created by Felix on 20.05.2014.
 */
public class Position {
    public int xPos;
    public int yPos;

    public Position(int xPos, int yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
    }

    @Override
    public String toString() {
        return String.format("[Position: x = %d, y = %d]", xPos, yPos);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        return xPos == position.xPos && yPos == position.yPos;

    }

    @Override
    public int hashCode() {
        int result = xPos;
        result = 31 * result + yPos + 1;
        return result;
    }
}
