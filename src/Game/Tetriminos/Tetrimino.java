package Game.Tetriminos;

import Game.Block;
import Game.GameField;
import javafx.scene.paint.Color;

import java.util.ArrayList;

/**
 * Created by Felix on 20.05.2014.
 */
public abstract class Tetrimino implements TetriminoInterface {
    public final int MAX_SIZE = 4;
    private GameField gameField;
    private Position position;
    private int curRotation;
    public Position[][] rotations;
    public Color color;

    private Block[] blocks;

    public Tetrimino(GameField gameField, Position position, int rotation) {
        this.gameField = gameField;
        this.position = position;
        curRotation = rotation;
        rotations = new Position[][]{
                {new Position(3, 3), new Position(-3, 3), new Position(3, -3), new Position(-3, -3)}
        };
        blocks = new Block[4];
        for (int i = 0; i < blocks.length; i++) {
            blocks[i] = gameField.createBlock(rotations[0][i]);
        }
    }

    public Tetrimino(GameField gameField, Position position) {
        this(gameField, position, 0);
    }

    @Override
    public int getTopBorder() {
        int topBorder = Integer.MAX_VALUE;

        for (Position position : rotations[curRotation]) {
            if (topBorder > position.yPos)
                topBorder = position.yPos;
        }

        return topBorder + position.yPos;
    }

    @Override
    public Position[] getRightBorder() {
        Position[] calcedPos = calcPosition(rotations[curRotation], position);
        Position[] leftBordersLong = new Position[4];
        for (Position pos : calcedPos) {
            System.out.println(pos.yPos % 4);
            if (leftBordersLong[(pos.yPos + MAX_SIZE) % MAX_SIZE] == null || leftBordersLong[(pos.yPos + MAX_SIZE) % MAX_SIZE].xPos < pos.xPos)
                leftBordersLong[(pos.yPos + MAX_SIZE) % MAX_SIZE] = pos;
        }

        ArrayList<Position> bottomBordersList = new ArrayList<>();
        for (Position pos : leftBordersLong) {
            if (pos != null) {
                bottomBordersList.add(pos);
            }
        }

        return bottomBordersList.toArray(new Position[0]);
    }

    @Override
    public Position[] getLeftBorder() {
        Position[] calcedPos = calcPosition(rotations[curRotation], position);
        Position[] leftBordersLong = new Position[4];
        for (Position pos : calcedPos) {
            System.out.println((pos.yPos + MAX_SIZE) % MAX_SIZE);
            if (leftBordersLong[(pos.yPos + MAX_SIZE) % MAX_SIZE] == null || leftBordersLong[(pos.yPos + 4) % 4].xPos > pos.xPos)
                leftBordersLong[(pos.yPos + MAX_SIZE) % MAX_SIZE] = pos;
        }

        ArrayList<Position> bottomBordersList = new ArrayList<>();
        for (Position pos : leftBordersLong) {
            if (pos != null) {
                bottomBordersList.add(pos);
            }
        }

        return bottomBordersList.toArray(new Position[0]);
    }

    @Override
    public Position[] getBottomBorder() {
        Position[] calcedPos = calcPosition(rotations[curRotation], position);
        Position[] bottomBordersLong = new Position[4];
        for (Position pos : calcedPos) {
            if (bottomBordersLong[(pos.xPos + MAX_SIZE) % MAX_SIZE] == null || bottomBordersLong[(pos.xPos + MAX_SIZE) % MAX_SIZE].yPos < pos.yPos)
                bottomBordersLong[(pos.xPos + MAX_SIZE) % MAX_SIZE] = pos;
        }

        ArrayList<Position> bottomBordersList = new ArrayList<>();
        for (Position pos : bottomBordersLong) {
            if (pos != null) {
                bottomBordersList.add(pos);
            }
        }

        return bottomBordersList.toArray(new Position[0]);
    }

    @Override
    public boolean moveLeft() {
        Position[] leftBorders = getLeftBorder();
        for (Position pos : leftBorders) {
            if (0 > pos.xPos - 1 || gameField.getGameFieldBlockMap().containsKey(new Position(pos.xPos - 1, pos.yPos))) {
                return false;
            }
        }
        position = new Position(position.xPos - 1, position.yPos);
        redrawBlocks();
        return true;
    }

    @Override
    public boolean moveRight() {
        Position[] rightBorders = getRightBorder();
        for (Position pos : rightBorders) {
            if (gameField.GAME_FIELD_WIDTH <= pos.xPos + 1 || gameField.getGameFieldBlockMap().containsKey(new Position(pos.xPos + 1, pos.yPos))) {
                return false;
            }
        }
        position = new Position(position.xPos + 1, position.yPos);
        redrawBlocks(true);
        return true;
    }

    @Override
    public boolean moveDown() {
        System.out.println(this + ": Move Down");
        for (Position position1 : gameField.getGameFieldBlockMap().keySet()) {
            System.out.print(position1 + ", ");
        }
        System.out.println();
        Position[] bottomBorders = getBottomBorder();
        for (Position pos : bottomBorders) {
            if (gameField.GAME_FIELD_HEIGHT <= pos.yPos + 1 || gameField.getGameFieldBlockMap().containsKey(new Position(pos.xPos, pos.yPos + 1))) {
//              System.out.println("gameField.GAME_FIELD_HEIGHT == pos.yPos+1 = " + (gameField.GAME_FIELD_HEIGHT == pos.yPos + 1));
//              System.out.println("gameField.getGameFieldBlockMap().containsKey(new Position(pos.xPos, pos.yPos + 1)) = " + (gameField.getGameFieldBlockMap().containsKey(new Position(pos.xPos, pos.yPos + 1))));
                return false;
            }
        }
        position = new Position(position.xPos, position.yPos + 1);
        redrawBlocks();
        return true;
    }

    @Override
    public boolean rotate() {
        System.out.println(this + ": Rotate");
        int newRotation = (curRotation + 1) % rotations.length;
        if (isFree(calcPosition(rotations[newRotation], position)))
            curRotation = newRotation;
        else if (isFree(calcPosition(rotations[newRotation], new Position(position.xPos, position.yPos - 1)))) {
            curRotation = newRotation;
            position.yPos -= 1;
        } else if (isFree(calcPosition(rotations[newRotation], new Position(position.xPos + 1, position.yPos)))) {
            curRotation = newRotation;
            position.xPos += 1;
        } else if (isFree(calcPosition(rotations[newRotation], new Position(position.xPos - 1, position.yPos)))) {
            curRotation = newRotation;
            position.xPos -= 1;
        } else if (isFree(calcPosition(rotations[newRotation], new Position(position.xPos + 2, position.yPos)))) {
            curRotation = newRotation;
            position.xPos += 2;
        } else if (isFree(calcPosition(rotations[newRotation], new Position(position.xPos - 2, position.yPos)))) {
            curRotation = newRotation;
            position.xPos -= 2;
        } else
            return false;
        redrawBlocks();
        for (Block block : blocks) {
            gameField.getGameFieldBlockMap().put(block.getPos(), block);
        }
        return true;
    }

    public void redrawBlocks() {
        redrawBlocks(false);
    }

    public void redrawBlocks(boolean reverse) {
        Position[] calcedPos = calcPosition(rotations[curRotation], position);
        if (reverse) {
            for (int i = blocks.length - 1; i >= 0; i--) {
                blocks[i].setPos(calcedPos[i]);
                blocks[i].setColor(color);
            }
        } else {
            for (int i = 0; i < blocks.length; i++) {
                blocks[i].setPos(calcedPos[i]);
                blocks[i].setColor(color);
            }
        }
    }

    private Position[] calcPosition(Position[] positions, Position pos) {
        Position[] calcedPos = new Position[positions.length];
        for (int i = 0; i < calcedPos.length; i++) {
            calcedPos[i] = new Position(positions[i].xPos + pos.xPos, positions[i].yPos + pos.yPos);
        }

        return calcedPos;
    }

    private boolean isFree(Position[] positions) {
        boolean ignoreBlock = false;
        for (Position pos : positions) {
            for (Block block : blocks) {
                if (block.getPos().equals(pos))
                    ignoreBlock = true;
            }
            if (!ignoreBlock && (pos.yPos >= gameField.GAME_FIELD_HEIGHT ||
                    pos.xPos < 0 || pos.xPos >= gameField.GAME_FIELD_WIDTH ||
                    gameField.getBlock(pos) != null))
                return false;
            ignoreBlock = false;
        }
        return true;
    }
}
