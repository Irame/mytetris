package Game.Tetriminos;

/**
 * Created by Felix on 20.05.2014.
 */
public interface TetriminoInterface {
    public boolean rotate();

    public boolean moveDown();

    public boolean moveLeft();

    public boolean moveRight();


    public Position[] getLeftBorder();

    public Position[] getRightBorder();

    public int getTopBorder();

    public Position[] getBottomBorder();
}
