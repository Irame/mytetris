package Game.Tetriminos;

import Game.GameField;
import javafx.scene.paint.Color;

/**
 * Created by Felix on 20.05.2014.
 */
public class Z extends Tetrimino {
    public Z(GameField gameField, Position position, int rotation) {
        super(gameField, position, rotation % 2);
        rotations = new Position[][]{        //[rotaionNum][blockNum] = Position
                {new Position(-1, 1), new Position(-1, 0), new Position(0, 0), new Position(0, -1)},
                {new Position(0, 1), new Position(1, 1), new Position(-1, 0), new Position(0, 0)}
        };
        color = Color.RED;
        redrawBlocks();
    }
}