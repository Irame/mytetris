package Game.Tetriminos;

import Game.GameField;
import javafx.scene.paint.Color;

/**
 * Created by Felix on 20.05.2014.
 */
public class O extends Tetrimino {
    public O(GameField gameField, Position position, int rotation) {
        super(gameField, position);
        rotations = new Position[][]{        //[rotaionNum][blockNum] = Position
                {new Position(0, 0), new Position(1, 0), new Position(0, -1), new Position(1, -1)}
        };
        color = Color.YELLOW;
        redrawBlocks();
    }
}