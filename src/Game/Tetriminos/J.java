package Game.Tetriminos;

import Game.GameField;
import javafx.scene.paint.Color;

/**
 * Created by Felix on 20.05.2014.
 */
public class J extends Tetrimino {
    public J(GameField gameField, Position position, int rotation) {
        super(gameField, position, rotation % 4);
        rotations = new Position[][]{        //[rotaionNum][blockNum] = Position
                {new Position(0, 1), new Position(0, 0), new Position(0, -1), new Position(1, -1)},      //  |´
                {new Position(1, 1), new Position(-1, 0), new Position(0, 0), new Position(1, 0)},     //  -,
                {new Position(-1, 1), new Position(0, 1), new Position(0, 0), new Position(0, -1)},    //  J
                {new Position(-1, 0), new Position(0, 0), new Position(1, 0), new Position(-1, -1)}      //  ´-
        };
        color = Color.BLUE;
        redrawBlocks();
    }
}