package Game;

/**
 * Created by Felix on 27.05.2014.
 */
public class Game {
    private double speed;
    private GameField gameField;
    private GameLoop gameLoop;
    private Thread gameLoopThread;
    private boolean isRunning;

    public Game(double speed) {
        this.speed = speed;

        gameField = new GameField();
        gameLoop = new GameLoop(this);
        gameLoopThread = new Thread(gameLoop);
    }

    public GameField getGameField() {
        return gameField;
    }

    public void start() {
        System.out.println("Start Game");
        gameLoop.isRunning = true;
        gameLoopThread.start();
    }


    public void rotateCurTetrimino() {
        if (gameLoop.curTetrimino != null)
            gameLoop.curTetrimino.rotate();
    }

    public void moveRightCurTetrimono() {
        if (gameLoop.curTetrimino != null)
            gameLoop.curTetrimino.moveRight();
    }

    public void moveLeftCurTetrimono() {
        if (gameLoop.curTetrimino != null)
            gameLoop.curTetrimino.moveLeft();
    }

    public void moveDownCurTetrimino() {
        if (gameLoop.curTetrimino != null)
            gameLoop.curTetrimino.moveDown();
    }

    public void moveToBottomCurTetrimino() {
        if (gameLoop.curTetrimino != null) {
            while (gameLoop.curTetrimino.moveDown()) ;
            gameLoopThread.interrupt();
        }
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
