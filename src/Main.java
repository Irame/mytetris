import GUI.GuiController;
import Game.Game;
import Game.Tetriminos.Position;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GUI/GUI.fxml"));
        Parent root = fxmlLoader.load();
        GuiController controller = fxmlLoader.getController();
        primaryStage.setTitle("Tetris");
        Scene scene = new Scene(root, 250, 400);

        Game game = new Game(0.5);

        scene.setOnKeyPressed((KeyEvent keyEvent) -> {
            if (keyEvent.getCode() == KeyCode.UP){
                game.rotateCurTetrimino();
            } else if (keyEvent.getCode() == KeyCode.RIGHT) {
                game.moveRightCurTetrimono();
            } else if (keyEvent.getCode() == KeyCode.LEFT){
                game.moveLeftCurTetrimono();
            } else if (keyEvent.getCode() == KeyCode.DOWN){
                game.moveDownCurTetrimino();
            } else if (keyEvent.getCode() == KeyCode.SPACE){
                game.moveToBottomCurTetrimino();
            } else if (keyEvent.getCode() == KeyCode.P){
                printField(game);
            }

        });

        controller.mainPain.getChildren().add(game.getGameField().getGameFieldParent());

        primaryStage.setScene(scene);
        primaryStage.show();

        game.start();
    }


    private void printField(Game game){
        for (int y = 0; y < game.getGameField().GAME_FIELD_HEIGHT; y++){
            for (int x = 0; x < game.getGameField().GAME_FIELD_WIDTH; x++){
                if (game.getGameField().getGameFieldBlockMap().get(new Position(x, y)) == null)
                    System.out.print("  ");
                else
                    System.out.print("X ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}